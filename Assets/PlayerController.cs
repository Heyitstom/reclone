﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	
	enum States {Normal, Climbing, Sliding};


	public float gravity = 10.0f;
    public float groundSpeed = 6f;
	public float airSpeed = 6f;
	public float jumpSpeed = 5.0f;

    public float mouseSensitivity = 5f;
    public float maxHeadRotation = 80.0f;

    public Transform head;

	private LayerMask jumpable;
    private float yVelocity = 0;
    private CharacterController controller;
    private float currentVerticalRotation = 0;
    private Vector3 moveVelocity = Vector3.zero;
	private States state = States.Normal;
	private Vector3 climbingDestination = Vector3.zero;

	// for later
	private bool bonusJump = false;

    // Use this for initialization
    void Start()
    {
		jumpable = LayerMask.GetMask ("Scenery");
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
		// TODO separate some things into different classes

        MouseLook();

        Jump();

        Gravity();

        LedgeClimb();

        Movement();
        
        Vector3 velocity = moveVelocity + yVelocity * Vector3.up;
        controller.Move(velocity * Time.deltaTime);

		MoveCleanUp ();
    }

    void Gravity()
	{
		yVelocity -= gravity * Time.deltaTime;
    }

    void LedgeClimb()
	{   
		if (state == States.Climbing) {
			// TODO climb (not teleport) towards destination

			// TODO if reached destination, state revert to normal

			controller.transform.position = climbingDestination + Vector3.up;
			state = States.Normal;
		}

		if (!Input.GetButton ("Jump")) {
			return;
		}

		// TODO check for walls in front, if hit use that wall position down to check for ledges

		Vector3 playerPos = controller.transform.position;

		RaycastHit hitInfo = new RaycastHit ();
		if (!Physics.Raycast (playerPos + transform.TransformDirection (0, 0.8f, 1), Vector3.down, out hitInfo, 1.7f, jumpable)) {
			return;
		}

		state = States.Climbing;
		climbingDestination = hitInfo.point;

    }


    // TODO not good enough literally terrible
    void Movement()
    {
		// TODO have max speed, which if above, cannot be added to (but can remain at that speed) 
		// TODO have tiny max speed if crouching/sliding
		// TODO have acceleration instead of flat speed, 
		// TODO have acceleration increase speed more at lower speeds, so momentum builds up faster
		// TODO have deceleration if not holding direction (or if sliding)
		// TODO have acceleration and deceleration less if in the air
		// ---------------------------------------------------------- downhill sliding todos, much less important
		// TODO have ground clamping so can run downhill without leaving ground
		// TODO have ground clamping while sliding pick up speed and increase clamp height as you go more and more downhill
		// TODO have sliding downhill transfer vertical momentum transfer into horizontal momentum if hill flattens out

        float speed = groundSpeed;
        if (!controller.isGrounded)
        {

			speed = airSpeed;
			// TODO currently disables movement changing in air to test wall jumping
			return;
        }

		moveVelocity = transform.TransformDirection(new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"))).normalized * speed;
        
    }

    void Jump()
    {
		if (!Input.GetButtonDown("Jump")) { return; }

		if (CanJump ()) {
			yVelocity = jumpSpeed;
			bonusJump = false;
		} else {
			WallJump ();
		}
			
        //TODO allow a little bit of leyway if they jump off ledge too late
    }

    void WallJump()
    {
		// TODO maybe increse jump radius from 0.6 

		Vector3 playerPos = controller.transform.position;
		// check for nearby walls
		Collider[] nearby = Physics.OverlapCapsule (playerPos + new Vector3 (0f, -0.5f, 0f),
			playerPos + new Vector3 (0f, 0.35f, 0f),
			0.6f, jumpable);

		// ignore self capsule and character controller
		if (nearby.Length == 0) {
			return;
		}

		Vector3 normal = NormalOfClosestCollider (nearby);

		ReflectOffWall (normal);

		yVelocity = jumpSpeed;


		// TODO 
		// check verticality of nearest side
		// if slope angle below 60 or above 120 then don't walljump
		// set horizontal angle = delta between sides normal and current velocity direction
		// set speed in that direction to be highest of current speed or default jump speed

		// TODO when implement momentum based movement, see how this effects walljumping 
    }

	void ReflectOffWall(Vector3 wallNormal){
		// TODO if angle too flat, bounce a little bit away from wall

		Vector3 facingDirection = transform.TransformDirection (Vector3.forward);
		// If facing wall, bounce away
		if (Vector3.Angle (facingDirection, wallNormal) < 90f) {
			moveVelocity = facingDirection;
		} else {
			moveVelocity = Vector3.Reflect (facingDirection, wallNormal);
		}

		moveVelocity = moveVelocity.normalized * airSpeed;
	}

	Vector3 NormalOfClosestCollider(Collider[] colliders){

		Vector3 playerPos = controller.transform.position;

		RaycastHit hitInfo = new RaycastHit();
		if (colliders.Length == 1) {
			Ray ray = new Ray (playerPos , (colliders [0].ClosestPointOnBounds (playerPos) - playerPos));
			Physics.Raycast(ray, out hitInfo, 2f, jumpable);
			return hitInfo.normal;
		}

		Collider closestCollider = colliders [0];
		float distanceB = Vector3.Distance (closestCollider.ClosestPointOnBounds (playerPos), playerPos);

		foreach (Collider collider in colliders)
		{
			Vector3 closestPointA = collider.ClosestPointOnBounds (playerPos);
			float distanceA = Vector3.Distance (closestPointA, playerPos);

			if (distanceA < distanceB)
			{
				closestCollider = collider;
				distanceB = distanceA;
			}
		}

		Physics.Raycast( new Ray (playerPos , (closestCollider.ClosestPointOnBounds (playerPos) - playerPos) ), out hitInfo, 2f, jumpable);

		return hitInfo.normal;
	}

    // because double jumps can potentially exist
    // will need separate code for walljumping that
    bool CanJump()
	{
		//TODO allow a little bit of leyway if they walk off ledge and jump too late
		return controller.isGrounded || bonusJump;
    }

    void MouseLook()
    {
        // mouse movement
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        transform.Rotate(Vector3.up, mouseInput.x * mouseSensitivity);
        
        // clamp vertical rotation
        currentVerticalRotation = Mathf.Clamp(currentVerticalRotation + mouseInput.y * mouseSensitivity, -maxHeadRotation, maxHeadRotation);

        head.localRotation = Quaternion.identity;
        head.Rotate(Vector3.left, currentVerticalRotation);
    }

	void MoveCleanUp(){
		if (controller.isGrounded) {
			yVelocity = 0;

			//TODO if sliding downhill don't do this
			//TODO also implement sliding + maybe a state machine
		}
	}
}
